# Cabo Cañaveral

## Puntos

* Puntuación Máxima `500`
* Puntuación Mínima `400`
* Decadencia `15`

## Pista

None

## Flag

`bitup20{houston_houston}`

## Adjuntos

* [cabo_cañaveral.zip](files/cabo_cañaveral.zip) (MD5: e8a6bde9887dbb549cef4b34f9e0676d)

## Deploy

None

## Descripcion

Dos fabricantes de cohetes compitiendo por conquistar el cielo, una acusación de espionaje industrial y un forense de guardia al que le va a tocar trabajar este fin de semana.

## Solucion

Como indica el README dentro del fichero comprimido, se deben encontrar pruebas claras de que Sebastian, trabajador de EFR Aerospace, ha filtrado los planos del cuerpo y del estabilizador del cohete a SpaceY, a cambio de una suma importante de dinero. Como apoyo, se aportan los planos originales del cuerpo del cohete.

El plano original del cuerpo del cohete es un archivo .obj, que puede ser visualizado en cualquier software CAD o diseño 3D. Al visualizarlo con el Visor 3D de Windows 10 se puede observar que tiene grabado "EFR Aerospace" en él.

![cohete-cuerpo-original.obj visto en el Visor 3D](media/1.png)

Respecto a las evidencias aportadas, las únicas comunicaciones encontradas de Sebastian con SpaceY son un chat de Whatsapp y un e-mail aparentemente inofensivo.

Analizando el chat de Whatsapp, parece bastante claro que Sebatian ha acordado recibir dinero de Andrés a cambio de "hacer algo":

```
[15/3/20 19:01:13] Andrés: Te has pensado lo que te comenté, Sebastián?
[15/3/20 19:01:13] Sebastián: Sí, lo haré
[...]
[16/3/20 15:59:45] Andrés: Me han autorizado 55k, es nuestra oferta final
[16/3/20 16:04:58] Sebastián: De acuerdo. Espero vuestra llamada con instrucciones
```

Además, se puede ver cómo Sebastian cifra los archivos .obj con un script llamado VCrypt.py, probablemente provisto por Andrés. También se le indica a Sebastian dónde tiene que meter los ficheros "para que no sospechen", lo que puede indicar el uso de alguna técnica esteganográfica.

```
[30/3/20 19:01:27] Sebastián: Andrés, sobre lo que hemos hablado. El comando que tengo que ejecutar primero es "python3 VCrypt.py cohete-estabilizador.obj > cohete-estabilizador-cifrado.obj"?
[30/3/20 19:01:51] Sebastián: Y para la otra mitad "python3 VCrypt.py cohete-cuerpo.obj > cohete-cuerpo-cifrado.obj"?
[30/3/20 19:56:21] Andrés: Correcto. Y luego ya sabes dónde los tienes que meter. Así no sospecharán. Esperamos tu e-mail.
```

Por último, tras pedirle a Sebastian que envíe un e-mail, él informa de que ya lo ha hecho y Andrés confirma su recepción.

```
[1/4/20 20:10:55] Sebastián: Enviado.
[1/4/20 20:20:16] Andrés: Recibido. Te ingresamos los 40k restantes.
```

Por otra parte, se dispone de un e-mail en formato .eml que puede ser abierto por un cliente como Evolution o Mozilla Thunderbird.

![Cena_reencuentro_UPV.eml abierto en Thunderbird](media/2.png)

Se puede observar que Sebastián Adell y Andrés Moreno fueron compañeros de carrera en la UPV. Se hace una referencia a los estabilizadores y, aunque el correo pueda parecer sin interés, si se extrae la imagen del .eml y se le hace un ataque de diccionario mediante stegcracker se descubre que existía una URL incrustada y cifrada con la contraseña "estrellitas".

![Resultado de ejecutar stegcracker sobre la firma del correo de Sebastián](media/3.png)

```
acmpxyz.com/a34564f54446b29fa28d126f50c1a1d0ad30ef73faf2d08759e7eb82e3535c27/planos.zip
```

Al acceder a la URL mediante http no se acepta la conexión, pero mediante https se descarga el fichero planos.zip, con ambos .obj cifrados con el programa VCrypt. Al intentar visualizar cualquiera de estos ficheros en el Visor 3D no se aprecia ningún objeto coherente.

![cohete-cuerpo-cifrado.obj visto en el Visor 3D](media/4.png)
![cohete-estabilizador-cifrado.obj visto en el Visor 3D](media/5.png)

Si se compara el fichero `cohete-cuerpo-cifrado.obj` con el original `cohete-cuerpo.obj`, se aprecian dos diferencias: La aparición del comentario `# Vertex coordinates encrypted with VCrypt` y que todos los vértices (líneas que comienzan por "v") tienen coordenadas diferentes. Al calcular la diferencia entre las coordenadas originales y las cifradas, se observa lo siguiente:

```
-120.043    +203.112    +33.22
+40.044     -22.444     +3.30
+59.932     +58.848     -50.11
-120.043    +203.112    +33.22
+40.044     -22.444     +3.30
+59.932     +58.848     -50.11
-120.043    +203.112    +33.22
+40.044     -22.444     +3.30
+59.932     +58.848     -50.11
[...]
```

Se puede apreciar el patrón de diferencias que se repite cada tres vértices:

```
-120.043    +203.112    +33.22
+40.044     -22.444     +3.30
+59.932     +58.848     -50.11
```

Estos son los valores que, sumándolos a las coordenadas originales, permiten convertirlo a coordenadas "cifradas". Por lo tanto, hay que restar estos valores a las coordenadas cifradas para obtener el fichero original. Se puede programar un script en Python para aplicar este proceso de descifrado:

```python
#!/usr/bin/env python3
import re
import sys

# Regex para detectar los vértices
line_filter = re.compile('^v ')

# Regex para detectar las coordenadas dentro de cada vértice
coord_filter = re.compile('([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)')

# Clave
offsets = [[-120.043, 203.112, 33.22],
           [40.044, -22.444, 3.30],
           [59.932, 58.848, -50.11]]
i_offset = 0
with open(sys.argv[1]) as f:
    print('# Vertex coordinates decrypted from VCrypt')
    for line in f.read().splitlines():
        if line_filter.match(line):
            # Si se trata de un vértice, obtener las coordenadas cifradas
            coords = [float(i[0]) for i in coord_filter.findall(line)]
            
            # Restar la fila i_offset de la clave a las coordenadas cifradas
            new_coords = [a-b for a, b in zip(coords, offsets[i_offset])]
            
            # Escribir las coordenadas del vértice descifradas
            new_line = 'v {x:.4f}\t\t\t{y:.4f}\t\t\t{z:.5f}'
            print(new_line.format(x=new_coords[0], y=new_coords[1], z=new_coords[2]))

            # Avanzar a la siguiente fila de la clave
            i_offset = (i_offset + 1) % 3
        else:
            # Si no es un vértice, no hacer nada
            print(line)
```

Al aplicar este script a `cohete-cuerpo-cifrado.obj`, se obtiene un fichero idéntico a `cohete-cuerpo.obj` en el visor 3D. Si se aplica a `cohete-estabilizador-cifrado.obj`, se puede visualizar el estabilizador con el flag grabado.

![Resultado de descifrar cohete-cuerpo-cifrado.obj, visualizado en Visor 3D](media/6.png)

## Referencias

* https://en.wikipedia.org/wiki/Wavefront_.obj_file
