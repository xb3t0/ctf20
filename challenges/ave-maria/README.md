# Ave Maria

## Puntos 

* Puntuación Máxima `400`
* Puntuación Mínima `300`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{BITUPTHEBESTCON}`

## Adjuntos

* [Challenge.pcapng](files/challenge.pcapng) (MD5: b0823b3171490aca188b5db1d22f1fdf)

## Deploy

None

## Descripcion

Descubrimos que un usuario identificado como "bitup" se ha descargado una imagen bastante sospechosa a través de un servidor FTP de la CON... Resuelve el reto, por dios.

## Solucion

Recibimos un fichero pcapng que podemos analizar con una herramienta GUI como Wireshark por ejemplo.

![Imagen1](capturas/captura1.png)

Vemos los paquetes que contengan como protocolo "FTP-DATA" y el comando "RETR la_virgen_bitup2020.jpg" para posteriormente, seguir el tráfico TCP, convertir los datos a Raw, para poder descargar la imagen.

![Imagen2](capturas/captura2.png)

Si vemos los metadatos de la imagen que nos hemos descargado, podemos observar que el "artista" que ha creado la foto, es una cadena encodeada en base64...

![Imagen3](capturas/captura3.png)

En linux podemos decodificar la cadena con el comando `echo "ZDBudF9iZjByYzNfdGgxcyEK" | base64 -d`. Lo que nos devolverá la cadena: "d0nt_bf0rc3_th1s!"

![Imagen4-1](capturas/captura4-1.png)

¿Qué podemos hacer a partir de aquí? Pues usar una herramienta como steghide para ver si se ha embebido algo en la imagen original...

![Imagen4](capturas/captura4.png)

Nos devuelve que hay un fichero kdbx (base de datos de KeePass)

![Imagen5](capturas/captura5.png)

Podemos utilizar una herramienta como keepass2john para extraer el hash, para luego posteriormente tratarlo con alguna herramienta como John.

![Imagen6](capturas/captura6.png)

Rompemos la contraseña (`sakura`) y vemos como hay una entrada en la BD con un enlace a anonfiles (https://anonfiles.com/ra1fj1d9p8/flag_mp3).

![Imagen7](capturas/captura7.png)

Este anonfiles, tiene un fichero llamado "flag.mp3". Si lo escuchamos atentamente, notamos que la mujer del audio está diciendo algo en el lenguaje NATO (alfabeto fonético) https://en.wikipedia.org/wiki/NATO_phonetic_alphabet.

También observamos que la mujer dice "caracteres" extraños como por ejemplo "dot" (.) o "slash" (/). 

Al "descifrar" esto, llegamos a este enlace: https://controlc.com/a750f645

![Imagen8](capturas/captura8.png)

Cuando entramos al enlace, vemos un galimatías ilegible. La pista que aparece es el "Ave maría" en latín. 

Si hacemos un poco de researching en cualquier motor de búsqueda, llegamos a la conclusión de que se trata del cifrado de sustitución Trithemius aplicado al rezo del Ave María. Lo que hace este cifrado, es reemplazar cada letra de una palabra, en un grupo de palabras haciéndolo parecer un poema/salmo. Una web muy interesante para descifrar esto es https://www.dcode.fr/trithemius-ave-maria

![Imagen9](capturas/captura9.png)

## Referencias

* https://en.wikipedia.org/wiki/NATO_phonetic_alphabet
* https://www.dcode.fr/trithemius-ave-maria
