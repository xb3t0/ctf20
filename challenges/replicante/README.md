# Replicante

## Puntos

* Puntuación Máxima `300`
* Puntuación Mínima `200`
* Decadencia `5`

## Pista

None

## Flag

`bitup20{R3pl1c4n7_R01_hun737}`

## Adjuntos

* [JOI_Deckcard.zip](files/JOI_Deckcard.zip) (MD5: af5eb7b9dcb05ac6223b16a7d723b34c)

## Deploy

None

## Descripcion

Uno de nuestros agentes BLADE RUNNER ha desaparecido mientras seguía la pista de un peligroso replicante. Lo único que tenemos es una imagen que nos mandó desde su equipo equipado con KALI LINUX. Localiza al replicante y localizarás el paradero de nuestro agente y recemos que siga con vida.

## Solucion

Se nos da la imagen sobre un comprimido [JOI_Deckcard.zip](files/JOI_Deckcard.zip) que se trata de la mítica imagen de la película Blade Runner 2049 donde se ve el holograma de JOI y el agente K. Es un diseño artístico que, si hacemos zoom, podemos ver que la palabra **RICKDECKCARD23** se repite. 
![Imagen1](capturas/captura3.png)
Esta es la contraseña que luego utilizaremos para nuestro archivo ZIP ofuscado dentro de la imagen. Para extraer el archivo ZIP de la imagen utilizaremos el comando `binwalk --extract JOI_Deckcard.jpg` luego al abrirlo nos pedirá la contraseña e introduciremos **RICKDECKCARD23**.
![Imagen1](capturas/captura4.png)
Nos aparecerán una foto llamada **DSC_0323.JPG**, un archivo **notas.txt** y un script en Python 3 llamado **replicant_detector.py**
![Imagen1](capturas/captura8.png)

Si ejecutamos el programa Python con el comando: `python3 replicant_detector.py` se nos abrirá un software detector de replicante que nos pedirá el nombre de usuario y contraseña para entrar al sistema y luego nos pedirá 3 datos para localizar al replicante y así obtener nuestra flag.

Para conseguir el nombre de usuario y contraseña, dentro de las notas, tenemos uno debajo de otro unos códigos llamados **REVLMjM=** y **Q1lCRVJQVU5L** que están codificados en base64 y se corresponden con **DEK23** y **CYBERPUNK**.
![Imagen1](capturas/captura5.png)
Luego se nos pedirá el primer dato que será el nombre de un replicante. Si vamos de nuevo a notas.txt vemos un conjunto de pares de números muy largo, es código hexadecimal que si convertimos a ASCII nos aparecerá la dirección de wikipedia de Rutger Hauer, el mítico actor fallecido el año pasado y cuyo nombre de replicante es **ROYBATTY**, el dato a introducir.
![Imagen1](capturas/captura7.png)
El segundo dato, nos solicita que hagamos caso a las indicaciones de la imagen adjunta que es un acertijo. Usando las coordenadas que nos aparecen en notas.txt buscamos resolver el acertijo. Son todos lugares donde se han rodado escenas de la película, menos uno de ellos que se trata de un viaje virtual en los interiores de la empresa creadora del juego CYBERPUNK 2077. Si buscamos en el interior, encontraremos imágenes relacionadas con el acertijo, hasta dar en una pared con el logo de la empresa que si nos fijamos en el hueco amarillo pone **CDPROJEKT** que es el dato a introducir.
![Imagen1](capturas/captura1.png)
Por último se nos pedirá fecha de la última recepción de bitcoins. Para ello en notas.txt encontramos algo parecido a un hash que nos puede llevar a confusión. Se trata de una dirección de recepción de una cartera bitcoin. Si vamos a la web https://blockchain.info e introducimos la dirección en la zona de busqueda nos aparecerán dos resultados, el primero de ellos es de BTC, pulsamos en él y vemos que se trata de los 0,0019 BTC que nos dicen, la fecha sería 200720 que es cuando se hizo. Por último esperamos y se nos mostrará la flag.
![Imagen1](capturas/captura2.png)

Otra forma de resolverlo es buscando en el código del programa de Python el mensaje codificado, lo he intentado poner lo más difícil posible, no sé si alguien lo conseguirá.
![Imagen1](capturas/captura6.png)
![Imagen1](capturas/captura9.png)

## Referencias

None
