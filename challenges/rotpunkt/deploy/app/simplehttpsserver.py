from http.server import HTTPServer, BaseHTTPRequestHandler, SimpleHTTPRequestHandler
import ssl, os, logging

web_dir = os.path.join(os.path.dirname(__file__), 'www')
os.chdir(web_dir)

httpd = HTTPServer(('0.0.0.0', 443), SimpleHTTPRequestHandler)

httpd.socket = ssl.wrap_socket (httpd.socket,certfile='../server.pem', server_side=True)

logging.info('Starting https server')

httpd.serve_forever()
