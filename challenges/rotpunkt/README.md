# Rotpunkt

## Puntos

* Puntuación Máxima `400`
* Puntuación Mínima `300`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{r4ns0mw4r3_0p3r4t0r_st4rt3r_p4ck_1s_4_l4mb0}`

## Adjuntos

[files/rotpunkt.zip](files/rotpunkt.zip) (MD5: fc07ea78f14a02b45aa8ca3fd895ef8e)

## Deploy

None

## Descripcion

Tenemos un binario ilegítimo que saca por pantalla un texto cifrado (parece que han distribuido la version de preproducción con mensajes de depuración e igual no está acabada). Cabe destacar que no ha sido detectado por el antivirus corporativo, sin embargo, nuestro sistema de IPS perimetral ha detectado la descarga anómala de dicho binario. El texto puede ser un IoC interesante para nuestro departamento de seguridad. Haz ingenieria inversa para sacar qué hace el binario. Sólo tienes que hacer eso... o no.

## Solucion

El primer paso para saber qué hace dicho binario es saber si está empaquetado con algún `packer` ([UPX](https://upx.github.io/), [ASPack](http://www.aspack.com/)]. Para ello, hemos utilizado la herramienta [PEiD](https://www.aldeid.com/wiki/PEiD) para analizar el binario y detectar las firmas posibles. 

![rotpunkt.exe con PEiD](media/rotpunkt-peid.PNG)

Nótese que no detecta la firma. Igual se trata de un binario implementado en algún lenguaje bastante moderno. Vamos a intentar analizarlo con la herramienta [PE-bear](https://hshrzd.wordpress.com/pe-bear/) para ver si tenemos más éxito.

![rotpunkt.exe con PE-bear](media/rotpunkt-pe-bear-sections.PNG)

Cabe destacar que [PE-bear](https://hshrzd.wordpress.com/pe-bear/) abre el binario sin problemas. Posee la cabecera MS-DOS 2.0 Compatible EXE de un ejecutable [PE](https://docs.microsoft.com/en-us/windows/win32/debug/pe-format) y tiene secciones esperadas (y otras más interesantes `/4`, `/19`

Cabe destacar que las secciones no presentan características de empaquetado,  por ejemplo, que el tamaño en crudo (*raw size*) sea menor que el tamaño ocupado en memoria (*virtual size*). En la pestaña *Imports* podemos observar que importa la biblioteca `kernel32.dll` y utiliza varias funciones. Antes de decompilarlo, es interesante ejecutar por consola. 

![Ejecución de rotpunkt.exe en Powershell](media/rotpunkt-ida-powershell.PNG)

> **_NOTE:_** Es importante matizar que si fuera una muestra de malware real, antes de ejecutarlo, es importante hacer una *snapshot* de la máquina virtual. Así también hacer otra *snapshot* de los valores de los registros de Windows (e.g. con [Regshot](https://github.com/Seabreg/Regshot)), y después, otra *snapshot* para saber si los valores han sido modificados.

Podemos observar que quiere descifrar una cadena, por lo tanto, vamos a ver si tiene la función de descifrado. De este modo, el siguiente paso es decompilar el binario con [IDA Freeware](https://www.hex-rays.com/products/ida/support/download_freeware/) para ver las distintas funciones implementadas.

![Funciones de rotpunkt.exe en IDA Freeware](media/rotpunkt-ida-main-func.PNG)

Cabe destacar dos funciones, `main_main` y `main_encryptUrl`. Es importante guardar el *offset* de estas dos funciones para después hacer una depuración con [x64dbg](https://x64dbg.com/#start). Por lo tanto, hacemos doble click en las dos y apuntamos su *offset* (`0x49f7b0` y `0x49f620` respectivamente)

![Offset de la función main de rotpunkt.exe](media/rotpunkt-ida-main-offset.PNG)
![Offset de la función encryptUrl de rotpunkt.exe](media/rotpunkt-ida-encrypt-offset.PNG)

Si vamos a la función `main_encryptUrl` y listamos las referencias cruzadas (dónde se llama) podemos notar que es llamada después del mensaje por pantalla (nótese la función `printf` en la siguiente imagen).

![Funciones de rotpunkt.exe en IDA Freeware](media/rotpunkt-ida-call-to-encryptUrl.PNG)

Por lo tanto, parece que llama a la función `main_encryptUrl` con la cadena de caracteres mostrada por pantalla. La función en cuestión es la mostrada a continuación.

![Función encryptUrl en IDA Freeware](media/rotpunkt-ida-encrypt-two-loops.PNG)

En el grafo podemos ver dos bucles (aunque el compilador puede haber introducido alguna optimización como el [desenrollado de bucles](https://www.d.umn.edu/~gshute/arch/loop-unrolling.xhtml)). Si ampliamos el grafo y vamos al bucle anidado podemos ver una operación `xor` que extrae un único `byte` de un direccionamiento de memoria.

![Bucle anidado de la función encryptUrl](media/rotpunkt-ida-encrypt-xor.PNG)

En este momento tenemos información suficiente para poder ir al depurador [x64dbg](https://x64dbg.com/#start) y poder extrapolar la información que hemos recabado. En primer lugar, cuando [x64dbg](https://x64dbg.com/#start) es ejecutado, debemos de poner los `breakpoints` de las funciones `main_main` y `main_encryptUrl` (`0x49f7b0` y `0x49f620` respectivamente).

![Breakpoint en la función main](media/rotpunkt-x64-set-br-main.PNG)
![Breakpoint en la función encryptUrl](media/rotpunkt-x64-set-br-encryptUrl.PNG)

Una vez realizado ésto ejecutamos el programa con `Run` y se alcanza la función `main_main`.

![Funcion main](media/rotpunkt-x64-main-func.PNG)

Después ejecutamos otra vez `Run` y llegamos a la función `main_encryptUrl`. 

![Funcion main](media/rotpunkt-x64-encryptUrl-func.PNG)

En el depurador se pueden ver un único bucle. Se debe al [desenrollado del bucle](https://www.d.umn.edu/~gshute/arch/loop-unrolling.xhtml) anidado, una técnica utilizada para ahorrar ciclos de CPU en las distintas etapas de la pipeline de un procesador. De este modo, vamos a poner un breakpoint en las instrucciones finales del bucle (`0x49f70c`). 

![Inicio del bucle en la función encryptUrl](media/rotpunkt-x64-first-loop-encryptUrl.PNG)

Este último nos hará ver la letra descifrada antes de que empiece la siguiente iteración. Ejecutamos `Run` y veremos en el registro `RDI`

![Registros con valores en la ejecución del bucle](media/rotpunkt-x64-first-loop-encrypt-letters-01.PNG)
![Registros con valores en la ejecución del bucle](media/rotpunkt-x64-first-loop-encrypt-letters-02.PNG)
![Registros con valores en la ejecución del bucle](media/rotpunkt-x64-first-loop-encrypt-letters-04.PNG)
![Registros con valores en la ejecución del bucle](media/rotpunkt-x64-first-loop-encrypt-letters-05.PNG)
![Registros con valores en la ejecución del bucle](media/rotpunkt-x64-first-loop-encrypt-letters-06.PNG)

Finalmente, nos muestra la cadena de caracteres `0xd4ed1be5/t4n4t0$.html` ¿Parece una URL? La primera parte está en hexadecimal... Sin embargo hay un fichero `.htm` en la última parte de la cadena. Nótese que si ponemos la dirección en nuestro navegador (con `https`) convertirá los dígitos de hexadecimal a octetos de una dirección IP, en este caso, `212.237.27.229`. La especificación de una dirección IP en un navegador puede debe expresarse en octetos en cualquier codificación ([Uniform Resource Locators (URL) - 2.2. URL Character Encoding Issues](https://tools.ietf.org/html/rfc1738)).

![Flag BitUp 2020 reto rotpunkt](media/rotpunkt-url-flag.PNG)

El código fuente del binario está implementado en [Golang](https://golang.org/) y puede verse en el fichero [main.go](files/main.go).

```golang
/*
Author: Luis Fueris
Date: September 24, 2020
Description: this sample code shows a function that encrypt/decrypt (please,
erase comments if you want to do cipher operation) an URL dynamically. It 
could be use to hide a C&C URL from static analysis
*/
package main

import (
    "fmt"
)

// URL length
var LEN = 23


// Implements XOR bitwise operation to URL string
//
func encryptUrl(url []string, key []byte) []byte {
    bLetter := make([]byte, 1)
    cipherLetter := make([]byte, 1)
    cipherUrl := make([]byte, LEN)

    for i, letter := range url {
        bLetter = []byte(letter)
        for j, bit := range bLetter {
            cipherLetter[j] = bit ^ key[j]
        }

        cipherUrl[i] = cipherLetter[0]
    }

    return cipherUrl
}

// Main program checks is a debugger is present and calls cipherUrl() function
// in order to decrypt malicious URL. It should be noted that XOR cipher uses
// same function to encrypt and decrypt so if you want to encrypt something, 
// please erase comments (oUrl var) and call encryptUrl() 
//
func main() {

    key := []byte{1, 0, 0, 1, 0, 1, 1, 0}
    //oUrl := []string{"0", "x", "d", "4", "e", "d", "1", "b", "e", "5","/", 
    //                            "t", "4", "n", "4", "t", "0", "$",".", "h",
    //                            "t", "m", "l"}
    cUrl := []string{ "1", "y", "e", "5", "d", "e", "0", "c", "d", "4", ".", 
                                  "u", "5", "o", "5", "u", "1", "%", "/", "i", 
                                  "u", "l", "m"}
    //fmt.Printf("[!] We are going to cipher %s string\n", oUrl)
    //cUrl := encryptUrl(oUrl, key)
    //fmt.Printf("[*] Cipher URL: %s\n", cUrl)

    fmt.Printf("[!] We are going to decipher %s string\n", cUrl)
    encryptUrl(cUrl, key)

    return 
}
```


## Referencias

* https://upx.github.io/
* http://www.aspack.com/
* https://www.aldeid.com/wiki/PEiD
* https://hshrzd.wordpress.com/pe-bear/
* https://docs.microsoft.com/en-us/windows/win32/debug/pe-format
* https://github.com/Seabreg/Regshot
* https://www.hex-rays.com/products/ida/support/download_freeware/
* https://x64dbg.com/#start
* https://www.d.umn.edu/~gshute/arch/loop-unrolling.xhtml
* https://tools.ietf.org/html/rfc1738
* https://golang.org/
* https://www.browserling.com/tools/ip-to-hex
