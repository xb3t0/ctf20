# Deploy

Construir el contenedor con: 

`sudo docker build -t "house-of-bitup" .`

Levantar el contenedor con:

`sudo docker run -d -p "0.0.0.0:7890:7890" -h "house-of-bitup" --name="house-of-bitup" house-of-bitup`

Y entonces puedes conectarte al puerto 7890 para verificar que esta funcionando:

`nc 127.0.0.1 7890`