# Space Signal

## Puntos

* Puntuación Máxima `200`
* Puntuación Mínima `100`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{4lw4y5_l0v3_r4di0}`

## Adjuntos

[files/signal.raw](files/signal.raw) (MD5: 0b32584ee5c09688dc9cb2f77b219383)

## Deploy

None

## Descripcion

Nuestro laboratorio recibió está señal procedente del espacio, nadie ha podido averiguar que contiene en su interior aunque evidentemente no es ruido espacial. Esperamos que puedas ayudar a nuestros técnicos con ésta labor.

## Solucion

El archivo signal.raw , aunque con la extensión raw es un simple audio en formato wav, se puede ver mediante el comando: `file signal.raw`

Con la salida:
```
signal.raw: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 44100 Hz
```

El archivo wav se puede reproducir con cualquier reproductor de audio y se aprecia que es una señal de SSTV, para decodificarla se puede usar la aplicación "Robot36 - SSTV Image Decoder" disponible para Android, reproduciendo en segundo plano el archivo wav.
Una vez decodificada la imagen se obtendrá una imagen similar a la siguiente:

![Imagen Decodificada](capturas/imagen.jpg)

El texto intferior es la flag codificada en base64 (sin el simbolo = al final).

## Notas

Existen multitud de programas opensource y gratuitos para Windows, Linux y Android para poder decodifcar la señal además del anteriormente mencionado para android.

## Referencias

* https://en.wikipedia.org/wiki/Slow-scan_television
* https://play.google.com/store/apps/details?id=xdsopl.robot36&hl=es_419&gl=US
