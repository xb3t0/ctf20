# Disassemble

## Puntos

* Puntuación Máxima `300`
* Puntuación Mínima `200`
* Decadencia `10`

## Pista

1. Las partes de la flag están asociadas a los números de la posición en otro idioma. (en japonés)
2. Son 4 partes.

## Flag

`bitup20{vas_a_strcmpwritec_la_loooz}`

## Adjuntos

[disassemble.aab](files/disassemble.aab) (MD5: b336f0341242e92ec03b52f80342d652)

## Deploy

Se dispone de proyecto Android Studio con código original en Java y C++ para generar el Android App Bundle. Para ello descargar el fichero [disassemble.7z](deploy/disassemble.7z), descomprimir e importarlo.

## Descripcion

Escondí en la aplicación una clave secreta, **4Tsu no bubun ni wakarete imasu**, tienes que encontrarlas para poder hacernos con la flag.

## Solucion

Descargamos el fichero disassemble.aab y lo descomprimimos. Extraemos la librería `/lib/<arquitectura>/libnative-lib.so`

Vamos a resolver en este caso el reto con [Ghidra](https://ghidra-sre.org/) y la arquitectura `armeabi-v7a`. La primera parte puede salir fácil con strings. En gHidra se puede hacer esto buscando en strings por el formato de la flag: `bitup`.

![primera_parte](media/1.png)

Por tanto, obtenemos la **primera parte** de la flag: `bitup20{vas_a`

Si miramos en las funciones que están declaradas en el ELF, se puede apreciar una con un nombre raro `niban`. Si se abre o decompila, se va a poder ver un String hardcodeado, que es la **segunda parte** de la flag: `_strcmp`

![segunda_parte](media/2.png)  

La tercera parte se encuentra de forma similar, pero en este caso la función a ver es `Java_com_bitup_avengers_disassemble_MainActivity_stringFromJNI`. Esta es la única función que se encuentra exportada para ser usada por JNI en el APK, por lo que es natural que una persona que reversee la aplicación encuentre esto.

En este caso si ve reversea la aplicación se va a ver algo así:

![tercera_parte](media/3.png)  

La variable anterior a la llamada de `pbVar1 = operator<<<std--__ndk1--char_traits<char>>((basic_ostream *)&DAT_0007918c,"writec");` es un array de bytes:

```
                             DAT_0008077f                                    XREF[1]:     Java_com_bitup_avengers_disassem
        0008077f 73              undefined1 73h	   s
        00080780 00              ??         00h
        00080781 61              ??         61h    a
        00080782 00              ??         00h
        00080783 6e              ??         6Eh    n
        00080784 00              ??         00h
        00080785 62              ??         62h    b
        00080786 00              ??         00h
        00080787 61              ??         61h    a
        00080788 00              ??         00h
        00080789 6e              ??         6Eh    n
        0008078a 00              ??         00h
```
Si observamos con atención, esta tiene el formato de un String pero separado por hex 00h, el cual es `sanban` (la tercera parte). Esto indica que la instrucción siguiente va a dar el valor de la tercera parte. En este caso la **tercera parte** de la flag es el string `writec`. 

Para la cuarta parte hay que hacer reversing de una función `yonban` (que es 4 en Japonés), porque de ahí sale la última parte. La función reverseada sería la siguiente:

```
void yonban(char* parte_1,char* parte_2) {

    std::cout << "parte_1" << std::endl;
    for (int i = 0; i < 6; i++) {
        int operador = parte_1[i] << i;
        operador += clave[6-i];
        std::cout << operador << "," << std::endl;
    }

    std::cout << "parte_2" << std::endl;
    for (int i = 0; i < 6; i++) {
        int operador = rand_vals[i] ^ parte_2[i];
        std::cout << operador << "," << std::endl;
    }
}
```
Ahora para obtener `parte_1` y `parte_2`, se necesita algo más, que es el output de la función que se ejecutó. Si se busca de nuevo por String 'parte_1', se va a poder ver el output de la función:

![cuarta_parte](media/4.png) 

Con ese output más la función, se puede generar una función inversa:

```
void inverse_yonban(int* parte_1,int* parte_2) {

    std::cout << "parte_1" << std::endl;
    for (int i = 0; i < 6; i++) {
	int operador = parte_1[i];       
	operador -= clave[6-i];
        operador = operador >> i;
        std::cout << operador << "," << std::endl;
    }

    std::cout << "parte_2" << std::endl;
    for (int i = 0; i < 6; i++) {
        int operador = rand_vals[i] ^ parte_2[i];
        std::cout << operador << "," << std::endl;
    }
}
```
`clave` y `rand_vals` se obtienen de las referencias al código. Lo que se hizo en este caso, para que fuera más fácil entender la estructura, es convertir de 'undefined' a int[x] cada uno de los dos, de modo que me queda así:

![cuarta_parte](media/5.png)

Después, esos valores se tienen que cambiar a decimal para usarlos en la aplicación, y deberían quedar así:

```
int clave[] = {294, 6840, 1304, 63049, 129, 9405, 3456, 126, 759, 9675};
int rand_vals[] = {45632, 45970, 194048, 14, 50};
```

Con esto ya se pueden procesar el resto de los valores:

```
void inverse_yonban(int* parte_1,int* parte_2) {

    std::cout << "parte_1" << std::endl;
    for (int i = 0; i < 6; i++) {
	int operador = parte_1[i];       
	operador -= clave[6-i];
        operador = operador >> i;
        std::cout << (char) operador;
    }

    std::cout << std::endl << "parte_2" << std::endl;
    for (int i = 0; i < 6; i++) {
        int operador = rand_vals[i] ^ parte_2[i];
        std::cout << (char) operador;
    }
}

int main()
{
    int parte_1[6] = {3551,9621,517,63809,3032,6840};
    int parte_2[6] = {45615,46077,194159,116,79,0};
    inverse_yonban(parte_1,parte_2);
}
```
Lo cual devolvería:

```
parte_1
_la_l
parte_2
oooz}
```

De ahí se concatena y se saca la **cuarta parte** de la flag: `_la_loooz}`

## Referencias

* https://ghidra-sre.org/
* https://ragingrock.com/AndroidAppRE/
* https://azeria-labs.com/writing-arm-assembly-part-1/
* https://training.azeria-labs.com/archive/
* https://malwareunicorn.org/workshops/re101.html#0
* https://developer.android.com/guide/app-bundle


