# Coming Home

## Puntos

* Puntuación Máxima `300`
* Puntuación Mínima `200`
* Decadencia `15`

## Pista

Ordenadas de menor a mayor coste de puntuación. (No añadir si no se va a restar puntuación por cada una de ellas):

* La respuesta está en... la respuesta :)
* Quién es el primero en darte la mano al entrar?
* Investiga cómo identifican la IP real la mayoría de backends tras pasar por un frontend (proxy reverso/balanceador de carga)
* Te gusta el contrabando?
* None None None... y si se la cuelas?

## Flag

`bitup20{ni_pr0xy_ni_pr0sis}`

## Adjuntos

* [config.zip](files/config.zip) (MD5: 94be051d2ae390b8bb9c4ba1b4d8e75a)

## Deploy

Haz click [aqui](deploy) para ver como desplegar el reto con Docker. 

## Requerimientos

Se requiere tener instalado **Docker** para poder desplegar el reto.

## Descripcion

I'm coming home
I'm coming home
Tell the world I'm coming home
Let the rain wash away all the pain of yesterday
I know my kingdom awaits and they've forgiven my mistakes
I'm coming home, I'm coming home
Tell the world that I'm coming
 
(Haciendo referencia HOME->127.0.0.1, visible también en el análisis del código dado)

## Solucion

En resumen, el reto consiste en la explotación de un HTTP Request Smuggling, para engañar a Haproxy y "colarle" un header "X-Forwarded-For" que no eliminará al estar en el cuerpo de la petición, y no en los headers.

Se identifica la infraestructura utilizada ya sea mediante los headers `Server: gunicorn/19.9.0 | X-Load-Balancer: haproxy 1.7.0` o el análisis de los ficheros otorgados, y al investigar un poco se llega a varios papers/posts hablando sobre una posible explotación del motor de Haproxy encargado de procesar los headers. ([Fuente](https://nathandavison.com/blog/haproxy-http-request-smuggling)). 

Gracias a esta vulnerabilidad y a la opción `http-reuse always` de `haproxy.cfg` (que indica la reutilización de los sockets), podemos "encolar" peticiones en el backend y conseguir craftear una petición final que cumpla todos los requisitos.

![Petición desde BurpSuite](capturas/reqs.png)

Como se puede observar, en la primera petición (Repeater) se está aprovechando el fallo anterior (utilizando \0b) para dejar una segunda petición medio formada, conteniendo el header que Haproxy stripea `http-response set-header X-Load-Balancer "haproxy 1.7.0"` en el body, sitio donde Haproxy no tiene configurado tocar nada. En la segunda pestaña (Intruder), se peticiona una petición lícita, sin nada extraño, que es la que queda "appendeada" a la anterior, y el backend recibe el header con los datos que toca.

## Notas

Al igual que se puede realizar Repeater->Intruder, se podría hacer al revés. En ese caso, si la potencia de dicho Intruder es muy grande, se podría estar dando la flag a cualquier petición lícita. No lo veo un gran problema, porque la primera petición no está acabada como tal y BurpSuite suele tirar timeout, pero puede pasar.

## Referencias

* https://nathandavison.com/blog/haproxy-http-request-smuggling
