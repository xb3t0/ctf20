# Deploy

Verificar el hash del export del contenedor (tienes el hash MD5 [aquí](../README.md)):

`md5sum wasm.zip`

Cargar el export del contenedor con: 

`sudo docker load -i wasm.zip`

Levantar el contenedor con:

`sudo docker run -d -p 80:80 wasm`

Y entonces puedes hacer una peticion a localhost:80 para verificar que esta funcionando:

`curl -v http://127.0.0.1:80`
