# WASMxtreMe

## Puntos

* Puntuación Máxima `500`
* Puntuación Mínima `400`
* Decadencia `5`

## Pista

None

## Flag

`bitupCTF2020{Full0fB3ans}`

## Adjuntos

None

## Deploy

* [wasm.zip](deploy/wasm.zip) (MD5: 7f7d641d68e78ccb0e67cf2aeaa44765)

Recuerda que puedes encontrar todos los archivos del despliegue en [deploy](deploy).

### Requerimientos

Se requiere tener instalado **Docker** para poder desplegar el reto.

## Descripcion

Adivina cuál es la cadena de entrada a la puerta que te dará la flag y consigue XXXX puntos.

https://wasmxtreme.bitupalicante.com

P.D: el formato flag para este reto es: `bitupCTF2020{xxxx}`

## Solucion

WASM: `B1tupCTFisE4sy`
flag: `bitupCTF2020{Full0fB3ans}`

## Referencias

None

