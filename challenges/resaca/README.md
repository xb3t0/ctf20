# Resaca

## Puntos

* Puntuación Máxima `300`
* Puntuación Mínima `200`
* Decadencia `5`

## Pista

None

## Flag

`bitup20{no_me_acuerdo_de_nada}`

## Adjuntos

* [image.bin](https://mega.nz/file/1JFyDZLC#GG6OsWWJcEp09qkmNm3yOwpH7BHSI4M63xZYLTMygZE) (MD5: c9e52f0c11500b6e93134e5034a9e0ee)

## Deploy

None

## Descripcion

¿Qué paso anoche? No me acuerdo de nada... Quizás este dump de memoria puede ayudarte...

https://mega.nz/file/1JFyDZLC#GG6OsWWJcEp09qkmNm3yOwpH7BHSI4M63xZYLTMygZE

## Solucion

En este challenge nos dan un archivo `image.bin`, que es un memory dump de una maquina virtual windows 7. 

Volatility identifica la version de windows, elegimos el primer perfil.

`python2 ~/Tools/volatility/vol.py -f ./image.bin imageinfo`

```
Suggested Profile(s) : Win7SP1x86_23418, Win7SP0x86, Win7SP1x86_24000, Win7SP1x86
```

Usando volatility podemos extraer una lista de los processos que se estaban ejecutando cuando se creo el memdump.

`python2 ~/Tools/volatility/vol.py -f ./image.bin --profile=Win7SP1x86_23418 pslist`

Vemos algunos procesos interesantes como stickynotes, firefox y chrome.

```
0x85cd5468 StikyNot.exe           2252   2144      8      138      1      0 2020-10-02 19:27:24 UTC+0000
0x84a0ed20 firefox.exe            2696   2832     69     1091      1      0 2020-10-02 19:41:05 UTC+0000                                 
0x85e20d20 firefox.exe            2592   2696     10      256      1      0 2020-10-02 19:41:05 UTC+0000                                 
0x85a894a0 firefox.exe            1112   2696     18      318      1      0 2020-10-02 19:41:05 UTC+0000                                 
0x84271748 firefox.exe            2304   2696     20      320      1      0 2020-10-02 19:41:06 UTC+0000                                 
0x85d6b980 firefox.exe            3024   2696      0 --------      1      0 2020-10-02 19:41:07 UTC+0000   2020-10-02 19:42:11 UTC+0000  
0x85d8bbd8 firefox.exe            1212   2696     21      343      1      0 2020-10-02 19:41:08 UTC+0000                                 
0x85d8b628 firefox.exe            2064   2696      0 --------      1      0 2020-10-02 19:41:13 UTC+0000   2020-10-02 19:43:47 UTC+0000  
0x85d30030 chrome.exe             3240   2144     28      722      1      0 2020-10-02 19:41:21 UTC+0000                                 
0x85a59430 chrome.exe              184   3240      8       76      1      0 2020-10-02 19:41:21 UTC+0000                                 
0x843168b8 chrome.exe             2088   3240     15      299      1      0 2020-10-02 19:41:22 UTC+0000                                 
0x8427c7a8 chrome.exe             3888   3240      9      202      1      0 2020-10-02 19:41:24 UTC+0000                                 
0x84391d20 chrome.exe             1032   3240     14      279      1      0 2020-10-02 19:41:28 UTC+0000                                 
0x8434fd20 chrome.exe             2672   3240     13      192      1      0 2020-10-02 19:41:28 UTC+0000                                 
0x842ca3d8 chrome.exe             3580   3240     13      174      1      0 2020-10-02 19:41:29 UTC+0000   
```

Para ver el contenido de la nota, debemos buscar un archivo `.snt`.

`python2 ~/Tools/volatility/vol.py -f image.bin --profile=Win7SP1x86_23418 filescan |grep .snt`
```
Volatility Foundation Volatility Framework 2.6.1
0x000000007e34a368      9      1 RW-r-- \Device\HarddiskVolume1\Users\IEUser\AppData\Roaming\Microsoft\Sticky Notes\StickyNotes.snt
0x000000007e739d58      8      0 R--r-d \Device\HarddiskVolume1\Windows\System32\sysntfy.dll
0x000000007ef6ac40      8      0 R--r-d \Device\HarddiskVolume1\Windows\System32\en-US\sntsearch.dll.mui
```
Ahora extraemos el fichero con dumpfiles

`python2 ~/Tools/volatility/vol.py -f image.bin --profile=Win7SP1x86_23418 dumpfiles -D ./dump -Q 0x000000007e34a368 -n`

Podemos usar `strings` para ver el contenido.

```
TO DO:
Cerrar session en congon4tor.me:7777
```

Usando el plugin de volatility [firefoxcookies](https://github.com/superponible/volatility-plugins) podemos ver las cookies que estan guardadas en el navegador (solo es compatible con firefox 26.0 o anterior...):

`python2 ~/Tools/volatility/vol.py --plugins=/home/congo/Tools/volatility/plugins/volatility-plugins/ -f ./image.bin --profile=Win7SP1x86_23418 firefoxcookies --output=csv | grep -v Failed`

```
"id","base_domain","app_id","inbrowserelement","name","value","host","path","expiry","last_accessed","creation_time","secure","httponly"
"7","congon4tor.me","0","0","bitup","OmFYUmN7dzNbPmFjbSJDcmhoVCooWjNbXzlTMickQ1k=","congon4tor.me","/","2020-12-31 20:45:50","2020-10-02 20:50:55.689000","2020-10-02 20:45:50.493000","0","0"
```

Visitando `congon4tor.me` con esa cookie conseguimos la flag:

`curl -H "Cookie: bitup=OmFYUmN7dzNbPmFjbSJDcmhoVCooWjNbXzlTMickQ1k=" http://congon4tor.me:7777`

## Referencias

* https://github.com/superponible/volatility-plugins
