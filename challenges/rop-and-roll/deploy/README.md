# Deploy

Construir el contenedor con: 

`sudo docker build -t "rop-and-roll" .`

Levantar el contenedor con:

`sudo docker run -d -p "0.0.0.0:7890:7890" -h "rop-and-roll" --name="rop-and-roll" rop-and-roll`

Y entonces puedes conectarte al puerto 7890 para verificar que esta funcionando:

`nc 127.0.0.1 7890`