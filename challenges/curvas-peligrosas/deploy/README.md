# Deploy

Construir el contenedor con: 

`sudo docker build -t "curvas-peligrosas" .`

Levantar el contenedor con (cambiar el `BASE_URL`, este debe ser la IP o dominio desde donde el jugador va a acceder al reto. El nombre se debe poder resolver desde dentro del contenedor):

`sudo docker run -d -p "0.0.0.0:8002:5000" --env BASE_URL=192.168.43.4:8002 --env FLAG="bitup20{mas_peligrosas_que_la_cruz_verde}" -h "curvas-peligrosas" --name="curvas-peligrosas" curvas-peligrosas`

Y entonces puedes conectarte al puerto 8002 para verificar que esta funcionando:

`curl http://127.0.0.1:8002`
