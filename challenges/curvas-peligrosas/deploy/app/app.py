from flask import Flask, request, render_template, make_response, abort, redirect, url_for
from jwcrypto import jwt, jwk
from os import environ
import json
import requests
import base64


app = Flask(__name__)

@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        # Get username from formdata
        username = request.form['username']
        if not username:
            return render_template('login.html', error="Debe introducir un nombre de usuario")
        
        if username == "admin":
            return render_template('login.html', error="No esta permitido iniciar session como admin")
        
        token = create_token(username)
        # Redirect to index and set JWT in cookies
        resp = make_response(redirect(url_for('index')))
        resp.set_cookie('curvas_peligrosas', token)
        return resp

    elif request.method == 'GET':
        return render_template('login.html')

@app.route('/')
def index():
    # Get jwt from cookie redirect to login if no jwt
    token = request.cookies.get('curvas_peligrosas')
    if not token:
        resp = make_response(redirect(url_for('login')))
        return resp

    flag = environ.get('FLAG',"error getting flag, report this error")
    jwt_token = verify_token(token)
    claims = json.loads(jwt_token.claims)
    return render_template('index.html', username=claims['username'], flag=flag)


@app.route('/logout', methods=['GET'])
def logout():
    # Redirect to logout and delete JWT in cookies
    resp = make_response(redirect(url_for('login')))
    resp.delete_cookie('curvas_peligrosas')
    return resp


# Create a JWT token
def create_token(username):

    # Get JKU from env var
    base_url = environ.get('BASE_URL',"localhost:5000")

    if not base_url:
        abort(500, "Variable BASE_URL not available in the environment, please export it.")

    header = {
        "alg": "ES256",
        "kid": "bitup20",
        "jku": "http://" + base_url + "/static/jwks.json"
    }

    # Initialize token
    token = jwt.JWT(header=header, claims={"username": username})

    # Sign token
    json_key = '{"crv":"P-256","d":"Yhe6oCUG6YqNvcdhKI1AVUWuRbxiWznsvZO8u6owAE4","kty":"EC","x":"DFvTjnvPqA88eX7cj5BNB5DDBKndVRAtk_D9MKDBTko","y":"dhJj9xk7daw9OhzpJ2r3zfoYA9TYB9ZX1mb0lALHsmw"}'
    key = jwk.JWK().from_json(json_key)
    token.make_signed_token(key)
    return token.serialize()

def verify_token(token):
    # Get jku from header, fix padding and base64 decode
    h = token.split('.')[0]
    h += '=' * (4 - (len(h) % 4))
    jku = json.loads(base64.urlsafe_b64decode(h)).get('jku', None)
    # Handle no jku header error
    if not jku:
        abort(400, "Header JKU no encontrado")

    # Get jku and generate jwk 
    try:
        r = requests.get(url=jku)
        keys = jwk.JWKSet.from_json(r.text)
    except Exception as e:
        abort(400, e)

    # Validate token and handle invalid signature
    try:
        jwt_token = jwt.JWT(key=keys, jwt=token)
        header = json.loads(jwt_token.header)
    except Exception as e:
        abort(400, e)

    return jwt_token
