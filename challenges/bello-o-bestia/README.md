# Bello o bestia

## Puntos

`50`

## Pista

Los códigos de barras están anticuados.

## Flag

`bitup20{V4ya_Bell3z0n}`

## Adjuntos

* [Bello_o_bestia.zip](files/Bello_o_bestia.zip) (MD5: 48698b76d8ce6bc32d8035ed12365a35)

## Deploy

None

## Descripcion

Hasta las mejores personas pueden esconder algo cuando todo parece perfecto. ¿Qué oculta Jomoza en esta imagen?

## Solucion

Cuando descargamos el zip del reto lo primero que tenemos que hacer es descomprimirlo.

![Descomprimiendo desde la terminal](capturas/descomprimir.png)

Si lo descomprimimos desde nuestra terminal vamos a darnos cuenta del archivo .secreto.txt pero si lo hacemos desde un gestor de archivos en el que tenemos desactivada la visualización de los archivos ocultos, lo pasaremos por alto. 

Al descomprimir los archivos nos queda una imagen jpg y archivo de texto previamente mencionado. La cadena de texto que contiene el txt nos va a servir para extraer otro archivo de texto de la imagen con steghide.

![Extrayendo información de la imagen](capturas/extraccion.png)

Si vemos el contenido del archivo que hemos conseguido, podemos identificar que se trata de algo pasado a base64. Vamos a decodear el archivo y lo guardamos con la extensión png. Ahora podemos abrir el archivo como imagen y descubrimos un código QR que nos indica la flag.

## Referencias

None