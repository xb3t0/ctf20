from django.contrib import admin

# Register your models here.
from .models import BankApp, Tokens

admin.site.register(BankApp)
admin.site.register(Tokens)
