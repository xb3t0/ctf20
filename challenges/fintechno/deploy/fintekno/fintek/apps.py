from django.apps import AppConfig


class FintekConfig(AppConfig):
    name = 'fintek'
