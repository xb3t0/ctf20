from django.db import models
from django.contrib.auth.models import User


class BankApp(models.Model):
    name = models.CharField(max_length=256)
    base_url = models.CharField(max_length=256)
    client_id = models.CharField(max_length=256)
    client_secret = models.CharField(max_length=256)


class Tokens(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    bank = models.ForeignKey(BankApp, on_delete=models.CASCADE)
    access_token = models.CharField(max_length=256)
    refresh_token = models.CharField(max_length=256)
