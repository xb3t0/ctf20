import requests


def get_transactions(token):
    response = requests.get(
        token.bank.base_url + "transactions/",
        headers={"Authorization": f"Bearer {token.access_token}"},
    )
    print(response)
    if response.status_code == 401:
        # refresh token and retry
        pass
    return response.json()
