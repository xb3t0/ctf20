from django.urls import path
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required

from .views import HomeView, ProfileView, GrantsView, RegisterView, logout

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path(
        "accounts/login/",
        LoginView.as_view(template_name="login.html"),
        name="login",
    ),
    path(
        "accounts/profile/",
        login_required(ProfileView.as_view()),
        name="profile",
    ),
    path("accounts/register/", RegisterView.as_view(), name="register"),
    path("grants/", login_required(GrantsView.as_view()), name="grants"),
    path("logout/", logout, name="logout"),
]
