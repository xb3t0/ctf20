from django.shortcuts import render, redirect
from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins
from oauth2_provider.contrib.rest_framework import (
    OAuth2Authentication,
    TokenHasScope,
)
from django.db.models import Q
from django.views.generic.edit import FormView
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth
from django.conf import settings


from .serializers import TransactionSerializer
from .models import Transaction

from oauth2_provider.models import Application, Grant


fintechno_app = Application.objects.filter(
    client_id="HArYAySdwNBhc1XOwCSYnbFaK29B3PXTcPmXHH3l"
).first()

Grant.objects.update(redirect_uri=settings.FINTECHNO_REDIRECT_URI)

fintechno_app.redirect_uris = settings.FINTECHNO_REDIRECT_URI
fintechno_app.save()


def home(request):
    if request.user.is_authenticated:
        return redirect("profile")
    return render(request, "home.html")


class TransactionViewSet(GenericViewSet, mixins.ListModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenHasScope]
    required_scopes = ["transactions"]
    serializer_class = TransactionSerializer

    def get_queryset(self):
        return Transaction.objects.filter(
            Q(from_user=self.request.user) | Q(to_user=self.request.user)
        )


class RegisterView(FormView):
    form_class = UserCreationForm
    success_url = "/"
    template_name = "login.html"

    def form_valid(self, form):
        u = User(username=form.cleaned_data["username"])
        u.set_password(form.cleaned_data["password1"])
        u.save()

        return super().form_valid(form)


def logout(request):
    auth.logout(request)
    return redirect("login")
