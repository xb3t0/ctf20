from django.contrib.auth.models import User
from django.db import models


class Transaction(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE)
    to_user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="from+"
    )
    description = models.TextField()
    ammount = models.IntegerField()
