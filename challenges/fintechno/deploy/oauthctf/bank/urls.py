from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView

from rest_framework.routers import SimpleRouter

from .views import home
from .views import TransactionViewSet, RegisterView, logout

router = SimpleRouter()
router.register("transactions", TransactionViewSet, basename="transaction")

urlpatterns = [
    path("", home),
    path(
        "accounts/login/",
        LoginView.as_view(template_name="login.html"),
        name="login",
    ),
    path(
        "accounts/profile/",
        login_required(
            TemplateView.as_view(template_name="site_is_down.html")
        ),
        name="profile",
    ),
    path("register/", RegisterView.as_view(), name="register"),
    path("logout/", logout, name="logout"),
] + router.urls
