# Kahlo v2

## Puntos

* Puntuación Máxima `500`
* Puntuación Mínima `400`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{FR1DA_ES_LO_M3J0R}`

## Adjuntos

[kahlo2.apk](files/kahlo2.apk) (MD5: f44490e1629913869120d0ae30a19a84)

## Deploy

Se dispone de proyecto Android Studio con código original en Kotlin para generar el APK. Para ello descargar el fichero [Kahlo2.7z](deploy/Kahlo2.7z), descomprimir e importarlo.

## Descripcion

Mi manager me pidió que hiciera rápido una demo de aplicación en Android para presentar al cliente. Como no tuve mucho tiempo, dejé la aplicación a medio terminar en el repositorio. ¿Me ayudarías a terminarla?.

## Solucion

Descargamos el APK y lo instalamos en nuestro dispositivo físico o un emulador con Genymotion usando ADB `adb install kahlo2.apk`. Al ejecutar la app nos encontramos con la siguiente pantalla:

![ejecutar_app](media/1.png)

Si presionamos el botón `TEST` aparentemente no hace nada, pero en los logs se aprecia información. Para ello consultamos el PID ejecutando `adb shell ps | grep kahlo` y ejecutamos el comando `adb logcat --pid=<<PID>>` donde se puede apreciar lo siguiente:

![logcat](media/2.png)

Vemos que nos indica, que debemos realizar una tarea con el tag `TODO` y que imprime algunos mensajes con pistas. Además, se observa una tag `PRUEBA----->` un poco sospechosa. Abrimos el APK con `Jadx-Gui` y nos vamos a la clase `MainActivity$onCreate$1`, vemos que se hace una llamada al método `decryptMessage` de la clase ```OfuscadorDividido``` y le pasa por parámetro el mensaje cifrado (R.string.message) y la key (R.string.clave). 

Si decompilamos la aplicación con `APKTool` y vamos a la ruta `res/values/strings.xml` y abrimos el fichero, podremos ver el contenido de las dos variables 'clave' y 'message', que son los usados para poder descifrar la Flag. Si probamos a decodificarla desde base64, nos daremos cuenta que no es posible al estar cifrada.

![decodificar](media/3.png)

Llegados a este punto, no nos queda otra, que ponernos en la mente del desarrollador e intentar adivinar el flujo con las pistas del logcat para terminar la aplicación y conseguir la Flag. Para ello, tendremos que usar Frida creando un script en Javascript hookeando todas las clases y métodos que intervienen en la operación de descifrado.

Después de pelearte un buen rato entendiendo el flujo de la aplicación, deberías llegar a un código como el siguiente alojado en la ruta [files/kahlo2_solucion.js](files/kahlo2_solucion.js)

```javascript
Java.perform( function () {
	
	var LaCaja = Java.use("com.bitup.kahlo2.encryption.LaCaja");
	var LaCajaCompanion = Java.use("com.bitup.kahlo2.encryption.LaCaja$Companion");
	
	LaCajaCompanion.setValue.implementation = function (pos, val) {
		LaCaja.intArray.value[pos] = val;
	}

	LaCajaCompanion.getValue.implementation = function (pos) {
		return LaCaja.intArray.value[pos];
	}

	LaCajaCompanion.swap.implementation = function (r1, r2) {
		var temp = LaCaja.intArray.value[r1];
		LaCaja.intArray.value[r1] = LaCaja.intArray.value[r2];
		LaCaja.intArray.value[r2] = temp;	
	}

	
	LaCajaCompanion.swap.setearCajaEnCero = function () {
		for (var i = 0; i < 256; i++) {
			LaCaja.intArray.value[i] = 0;
		}		
	}

	var LaClave = Java.use("com.bitup.kahlo2.encryption.LaClave");
	
	LaClave.getLongitudBytes.implementation = function () {
		return 255;	
	}

	
	LaClave.setKey.implementation = function (arrayBytes) {
		this.key.value = arrayBytes;
	}

	var OfuscadorDividido = Java.use("com.bitup.kahlo2.encryption.OfuscadorDividido");
	var String = Java.use("java.lang.String");

	OfuscadorDividido.decryptMessage.overload('[B', 'java.nio.charset.Charset', 'java.lang.String').implementation = function (message,charset,key) {
		
		this.reset();
		this.setKey(key);
		var msg = this.crypt(message);
		this.reset();
		return String.$new(msg,charset);			
	}
		
	OfuscadorDividido.moduleOperation.implementation = function (r1, r2) {
		return r1 % r2;	
	}
});
```

Una vez hecho esto, se puede ejecutar con el siguiente comando ```frida -U com.bitup.kahlo2 -l .\kahlo2_solucion.js```, luego en la aplicación presionamos el botón ```TEST``` y observamos lo que pasa en el logcat para conseguir la flag.

![flag](media/4.png)

## Referencias

* https://frida.re/
* https://ibotpeaches.github.io/Apktool/
* https://github.com/skylot/jadx
* https://developer.android.com/

