# TokeBit

## Puntos

- Flag Web Máximo: `50`
- Flag Web Mínimo: `10`
- Flag Web Decadencia: `10`

- Flag User Máximo: `100`
- Flag User Mínimo: `50`
- Flag User Decadencia: `10`

- Flag Root Máximo: `100`
- Flag Root Mínimo: `50`
- Flag Root Decadencia: `10`

- Flag Nodo Bitcoin Máximo: `50`
- Flag Nodo Bitcoin Mínimo: `10`
- Flag Nodo Bitcoin Decadencia: `5`

- Flag Mensaje oculto (Extra Ball) Máximo: `50`
- Flag Mensaje oculto (Extra Ball) Mínimo: `10`
- Flag Mensaje oculto (Extra Ball) Decadencia: `5`

- Flag Stegoanalisis Máximo: `50`
- Flag Stegoanalisis Mínimo: `10`
- Flag Stegoanalisis Decadencia: `5`

## Pista

None

## Flag

- **WEB:** `bitup20{mala_gestion_passwords}` 
- **USER**: `bitup20{con_wordpress_y_a_lo_loco}`
- **ROOT**: `bitup20{no_hay_acertijos_que_valgan}`
- **Nodo Bitcoin**: `bitup20{game_over_e.nigma}`
- **Mensaje oculto (Extra Ball)**: `bitup20{jack_is_back}`
- **Stegoanalisis**: `bitup20{la_oscuridad_no_es_segura}`

## Adjuntos

Enlaces de descarga de la máquina virtual:

- **MEGA:** https://mega.nz/file/KBkF0IrI#eBgrsG9l8jyGKHIwxbkVBzTMfSm7-z_TDjb5v30xMPc
- **Google Drive:** https://drive.google.com/file/d/1nMnIc3qgkerud1hH9KZJs3FXBH96tMs7/view?usp=sharing

## Deploy

Importar la máquina virtual a VirtualBox y arrancarla. La máquina virtual tiene una IP estática, la cual es **192.168.1.174**, debereís de poner vuestro equipo en esa misma subred para poder verla.

## Descripcion

Parece ser que BitUp y E-Corp han unido sus fuerzas para crear un nodo de la moneda E-Coin, la cual E-Corp intenta impulsar a escala global, pero un misterioso cracker ha irrumpido en el servidor y les ha estropeado la fiesta de lanzamiento, y parece que si queremos recuperar el servidor de vuelta, tendremos que deshacer lo que ha hecho, lo cual, implica volver a tomar el control total del mismo.

## Solucion

Tras iniciar la página, iremos a un navegador web y teclearemos la dirección IP de la máquina. 
Nos aparecerá un Wordpress con un tema por defecto pero con contenido insertado.

![](https://i.imgur.com/x8bk9h2.png)

Aquí se nos planteará un primer acertijo, y podremos ver el mensaje que el misterioso E.Nigma nos ha dejado.

Pasemos a la acción. Vayamos a nuestra Kali y lo primero que haremos será ejecutar WPScan para ver que obtenemos de la web.

```
macaco@whkali:~$ wpscan --url 192.168.1.174 --api-token zSsATC2zAFb9qZsgn0G1Bn1KgELjvfNHEaSYGsk7FHs
_______________________________________________________________
         __          _______   _____
         \ \        / /  __ \ / ____|
          \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
           \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
            \  /\  /  | |     ____) | (__| (_| | | | |
             \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.6
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
_______________________________________________________________

[+] URL: http://192.168.1.174/ [192.168.1.174]
[+] Started: Mon Oct 19 17:46:05 2020

Interesting Finding(s):

[+] Headers
 | Interesting Entry: Server: Apache/2.4.41 (Ubuntu)
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] XML-RPC seems to be enabled: http://192.168.1.174/xmlrpc.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%
 | References:
 |  - http://codex.wordpress.org/XML-RPC_Pingback_API
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_ghost_scanner
 |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/wordpress_xmlrpc_dos
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_xmlrpc_login
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_pingback_access

[+] WordPress readme found: http://192.168.1.174/readme.html
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] Upload directory has listing enabled: http://192.168.1.174/wp-content/uploads/
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] The external WP-Cron seems to be enabled: http://192.168.1.174/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 4.7.18 identified (Latest, released on 2020-06-10).
 | Found By: Rss Generator (Passive Detection)
 |  - http://192.168.1.174/index.php/feed/, <generator>https://wordpress.org/?v=4.7.18</generator>
 |  - http://192.168.1.174/index.php/comments/feed/, <generator>https://wordpress.org/?v=4.7.18</generator>

[+] WordPress theme in use: twentyseventeen
 | Location: http://192.168.1.174/wp-content/themes/twentyseventeen/
 | Last Updated: 2020-08-11T00:00:00.000Z
 | Readme: http://192.168.1.174/wp-content/themes/twentyseventeen/README.txt
 | [!] The version is out of date, the latest version is 2.4
 | Style URL: http://192.168.1.174/wp-content/themes/twentyseventeen/style.css?ver=4.7.18
 | Style Name: Twenty Seventeen
 | Style URI: https://wordpress.org/themes/twentyseventeen/
 | Description: Twenty Seventeen brings your site to life with header video and immersive featured images. With a fo...
 | Author: the WordPress team
 | Author URI: https://wordpress.org/
 |
 | Found By: Css Style In Homepage (Passive Detection)
 |
 | Version: 1.0 (80% confidence)
 | Found By: Style (Passive Detection)
 |  - http://192.168.1.174/wp-content/themes/twentyseventeen/style.css?ver=4.7.18, Match: 'Version: 1.0'

[+] Enumerating All Plugins (via Passive Methods)
[+] Checking Plugin Versions (via Passive and Aggressive Methods)

[i] Plugin(s) Identified:

[+] email-subscribers
 | Location: http://192.168.1.174/wp-content/plugins/email-subscribers/
 | Last Updated: 2020-09-25T07:27:00.000Z
 | [!] The version is out of date, the latest version is 4.6.0
 |
 | Found By: Urls In Homepage (Passive Detection)
 |
 | [!] 5 vulnerabilities identified:
 |
 | [!] Title: Email Subscribers & Newsletters < 4.2.3 - Multiple Issues
 |     Fixed in: 4.2.3
 |     References:
 |      - https://wpvulndb.com/vulnerabilities/9946
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19985
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19984
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19982
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19981
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19980
 |      - https://www.wordfence.com/blog/2019/11/multiple-vulnerabilities-patched-in-email-subscribers-newsletters-plugin/
 |      - https://cxsecurity.com/issue/WLB-2020080034
 |
 | [!] Title: Email Subscribers & Newsletters < 4.3.1 - Unauthenticated Blind SQL Injection
 |     Fixed in: 4.3.1
 |     References:
 |      - https://wpvulndb.com/vulnerabilities/9947
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20361
 |      - https://www.wordfence.com/blog/2019/11/multiple-vulnerabilities-patched-in-email-subscribers-newsletters-plugin/
 |
 | [!] Title: Email Subscribers & Newsletters < 4.5.1 - Cross-site Request Forgery in send_test_email()
 |     Fixed in: 4.5.1
 |     References:
 |      - https://wpvulndb.com/vulnerabilities/10321
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-5767
 |      - https://www.tenable.com/security/research/tra-2020-44-0
 |
 | [!] Title: Email Subscribers & Newsletters < 4.5.1 - Authenticated SQL injection in es_newsletters_settings_callback()
 |     Fixed in: 4.5.1
 |     References:
 |      - https://wpvulndb.com/vulnerabilities/10322
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-5768
 |      - https://www.tenable.com/security/research/tra-2020-44-0
 |
 | [!] Title: Email Subscribers & Newsletters < 4.5.6 - Unauthenticated email forgery/spoofing
 |     Fixed in: 4.5.6
 |     References:
 |      - https://wpvulndb.com/vulnerabilities/10403
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-5780
 |      - https://www.tenable.com/security/research/tra-2020-53
 |      - https://portswigger.net/daily-swig/vulnerability-in-wordpress-email-marketing-plugin-patched
 |
 | Version: 4.2.2 (100% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - http://192.168.1.174/wp-content/plugins/email-subscribers/readme.txt
 | Confirmed By: Readme - ChangeLog Section (Aggressive Detection)
 |  - http://192.168.1.174/wp-content/plugins/email-subscribers/readme.txt

[+] Enumerating Config Backups (via Passive and Aggressive Methods)
 Checking Config Backups - Time: 00:00:00 <=================================================> (21 / 21) 100.00% Time: 00:00:00

[i] No Config Backups Found.

[+] WPVulnDB API OK
 | Plan: free
 | Requests Done (during the scan): 3
 | Requests Remaining: 47

[+] Finished: Mon Oct 19 17:46:09 2020
[+] Requests Done: 58
[+] Cached Requests: 5
[+] Data Sent: 13.597 KB
[+] Data Received: 335.552 KB
[+] Memory used: 177.816 MB
[+] Elapsed time: 00:00:04
```
Con esto, obtendremos varias vulnerabilidades del Wordpress, concretamente, estas:
- Email Subscribers & Newsletters < 4.3.1 - Unauthenticated Blind SQL Injection
- Email Subscribers & Newsletters < 4.5.1 - Authenticated SQL injection in es_newsletters_settings_callback()

Pero, esta no es la única. Parece que WPScan no es capaz de detectarla aún, (o no lo tengo actualizado) así que veamos como detectarla de forma manual.

La vulnerabilidad es reciente, se trata de una vulnerabilidad en wp-file-manager que permite que un atacante no autenticado en el Wordpress, pueda subir ficheros de cualquier extensión sin problema alguno (CVE-2020-25213).

Para saber si el plugin existe, tendremos que introducir en el navegador web la siguiente ruta, junto con la IP de la máquina virtual: http://192.168.1.174/wp-content/plugins/wp-file-manager/lib/php/

Si nos aparece un listing de los directorios como este, entonces es que la web posee el plugin:

![](https://i.imgur.com/6sNNrZL.png)

Ahora vayamos a la parte de explotación. Según se nos indica en el articulo (https://medium.com/bugbountywriteup/exploiting-cve-2020-25213-wp-file-manager-wordpress-plugin-6-9-3f79241f0cd8), tendremos que hostear en local una copia de elFinder, y subir un fichero de imagen, para interceptar la peticion POST en Burp Suite. Una vez interceptada, cambiaremos la cabecera de la peticion POST para que en vez de apuntar a nuestro elFinder en local, apunte al del Wordpress (192.168.1.174/wp-content/plugins/wp-file-manager/lib/php/connector.minimal.php) y poder subir el fichero de forma correcta. 

Los ficheros subidos se pueden encontrar en la ruta 192.168.1.174/wp-content/plugins/wp-file-manager/lib/files/

En la misma web enlazada, hay un script de Bash que nos podemos descargar y que facilita la explotación de esta vulnerabilidad.




