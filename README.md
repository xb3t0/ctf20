# Bitup20

Proyecto de Documentación del CTF del Evento de Bitup 2020.

## Index

### Atomics
* [**hello-world**](challenges/hello-world/)
* [**demiguise**](challenges/demiguise/)
* [**hexa**](challenges/hexa/)
* [**helius-5**](challenges/helius-5/)
* [**un-ciudadano-ejemplar**](challenges/un-ciudadano-ejemplar/)

### Boot2Root
* [**tokebit**](challenges/tokebit/)

### Forensics
* [**guess-the-hash**](challenges/guess-the-hash/)
* [**resaca**](challenges/resaca/)

### Misc
* [**savito**](challenges/savito/)
* [**insomnia**](challenges/insomnia/)
* [**ave-maria**](challenges/ave-maria/)

### Mobile
* [**find-me**](challenges/find-me/)
* [**disassemble**](challenges/disassemble/)
* [**kahlo-v2**](challenges/kahlo-v2/)

### Pwn
* [**house-of-bitup**](challenges/house-of-bitup/)
* [**rop-and-roll**](challenges/rop-and-roll/)

### Radio
* [**space-signal**](challenges/space-signal/)

### Reversing
* [**rotpunkt**](challenges/rotpunkt/)

### Stego
* [**bello-o-bestia**](challenges/bello-o-bestia/)
* [**replicante**](challenges/replicante/)
* [**cabo-cañaveral**](challenges/cabo-cañaveral/)

### Web
* [**fintechno**](challenges/fintechno/)
* [**curvas-peligrosas**](challenges/curvas-peligrosas/)
* [**coming-home**](challenges/coming-home/)
* [**wafxtreme**](challenges/wafxtreme/)
* [**wasmxtreme**](challenges/wasmxtreme/)

## Descripcion

El CTF de este año, no hubiera sido posible sin la ayuda de todos esos contribuidores que han aportado sus retos, retos muy épicos y brutales, y nos han permitido hacer de esta edicción online, más divertida y llena de magia. ¡Qué le den al Covid! H4ck th3 W0rld!

**Importante:** si resolvisteis un reto y el writeup publicado no se parece a lo qué hicisteis o te parece que tu resolución es diferente, no dudes en contactar con @Secury (telegram) para añadir tu writeup a reto! Por otro lado, si detectas algún problema o algo que no es correcto, sientete libre de abrir un issue y lo solucionaremos ASAP!

## Deploy

Algunos de los retos que figuran en este repositorio no requieren de guia de despliegue, esto puede deberse a que utilizan archivos adjuntos donde figura la flag o directamente que la flag proviene de algún otro lado o irá contenida en la descripción del propio reto.

Ahora bien, para los que si necesitan de un proceso de despliegue (aquellos con estructura de deploy), serán orquestados utilizando Docker y Docker-Compose. Cada uno de estos archivos tiene un Dockerfile para construir la imagen del reto, entonces, para poder desplegar todos los retos a la vez, se ha creado un archivo [docker-compose.yml](challenges/docker-compose.yml) que permitirá construir las imágenes de todos los retos y desplegarlos.

El primer paso es cargar las imagenes de los Retos que disponen de una imagen en vez de un archivo de deploy, estos son: [wafxtreme](challenges/wafxtreme/deploy) y [wasmxtreme](challenges/wasmxtreme/deploy). Se debe entrar a cada uno de los retos, a la sección de deploy e importar las imágenes de los exports de los contenedores para poder desplegar todos los retos.

Una vez hecho esto, vamos a desplegar todos los retos de forma automática, para ello simplemente se debe dirigirse a la carpeta donde figura el archivo **docker-compose.yml**, y ejecutar:

```
sudo docker-compose build
```

Este comando construirá todos los contenedores de los retos. Y a continuación:

```
sudo docker-compose up -d
```

Este comando levantará todos los contenedores. Si, por ejemplo, se quiere levantar uno en particular, como lo es el reto de "coming-home", se puede hacer lo siguiente:

```
sudo docker-compose start comming-home
```

Si se quiere detener un contenedor de un reto pues lo mismo pero en vez de "start", "stop". Y si se quiere reiniciar pues "restart".

Y por último. Todos y cada uno de los retos, que precisan de exponer un puerto interno hacia fuera, se ha utilizado un número de puerto secuencial en orden ascendente, por consiguiente, no debería haber conflicto con ningún puerto.

## Contribuciones

* [hello-world](challenges/hello-world) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [demiguise](challenges/demiguise) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [hexa](challenges/hexa) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [helius](challenges/helius) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [un-ciudadano-ejemplar](challenges/un-ciudadano-ejemplar) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [tokebit](challenges/tokebit) desarrollado por TFujiwara86 aka [@TFujiwara86](https://twitter.com/TFujiwara86)
* [guess-the-hash](challenges/guess-the-hash) desarrollado por Álex aka [@r1p](https://twitter.com/r1p)
* [resaca](challenges/resaca) desarrollado por Ignacio Dominguez aka [@Congon4tor](https://twitter.com/Congon4tor)
* [savito](challenges/savito) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [insomnia](challenges/insomnia) desarrollado por Raúl Caro aka [@secu_x11](https://twitter.com/secu_x11)
* [ave-maria](challenges/ave-maria) desarrollado por Dres1 aka [@DreSecX](https://twitter.com/DreSecX)
* [find-me](challenges/find-me) desarrollado por MobileHackingES aka [@MobileHackingES](https://twitter.com/MobileHackingES)
* [disassemble](challenges/disassemble) desarrollado por MobileHackingES aka [@MobileHackingES](https://twitter.com/MobileHackingES)
* [kahlo-v2](challenges/kahlo-v2) desarrollado por MobileHackingES aka [@MobileHackingES](https://twitter.com/MobileHackingES)
* [house-of-bitup](challenges/house-of-bitup) desarrollado por C4ebt aka [@c4ebt](https://twitter.com/c4ebt)
* [rop-and-roll](challenges/rop-and-roll) desarrollado por Mrp314 aka [@_mrp314](https://twitter.com/_mrp314)
* [space-signal](challenges/space-signal) desarrollado por Ernesto aka [@ernesto_xload](https://twitter.com/ernesto_xload)
* [rotpunkt](challenges/rotpunkt) desarrollado por Luis Fueris aka [@lfm3773](https://twitter.com/lfm3773)
* [bello-o-bestia](challenges/bello-o-bestia) desarrollado por Daniel aka [@whoissecure](https://twitter.com/WhoIsSecure)
* [replicante](challenges/replicante) desarrollado por Rick Deckard aka [@rickdeckard23](https://twitter.com/rickdeckard23)
* [cabo-cañaveral](challenges/cabo-cañaveral) desarrollado por Havocsec aka [@havocsec](https://twitter.com/@havocsec)
* [fintechno](challenges/fintechno) desarrollado por ElChicoDePython aka [@ElChicoDePython](https://twitter.com/@ElChicoDePython)
* [curvas-peligrosas](challenges/curvas-peligrosas) desarrollado por Ignacio Dominguez aka [@Congon4tor](https://twitter.com/@Congon4tor)
* [coming-home](challenges/coming-home) desarrollado por JorgeCTF aka [@jorge_ctf](https://twitter.com/@jorge_ctf)
* [wafxtreme](challenges/wafxtreme) desarrollado por NN2ed_s4ur0n aka [@NN2ed_s4ur0n](https://twitter.com/@NN2ed_s4ur0n)
* [wasmxtreme](challenges/wasmxtreme) desarrollado por NN2ed_s4ur0n aka [@NN2ed_s4ur0n](https://twitter.com/@NN2ed_s4ur0n)
